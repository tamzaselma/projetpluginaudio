# ProjetPluginAudio

##Start the aplication 
ng serve
npm install -g json-server
json-server --watch src/db.json



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Implemented features

###General
- login as admin (admin/admin) or regular users-authors (guest/guest);
- logout
###Plugins
- displaying a list of plugins;
- pagination;
- search by clicking on tags;
- search by clicking on author;
- search within plugin text content (author, plugin name, description, tags) using a search bar;
- search by multiple words using a search bar;
- adding plugins;
- editing plugins;
- deleting plugins by their authors or admin;
- displaying plugin details
###Pedalboards
- displaying a list of pedalboards;
- adding pedalboards;
- search within pedalboard text content (author, pedalboard name, description) using a search bar;
- search by multiple words using the a bar;
- displaying pedalboard details


