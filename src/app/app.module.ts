import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import {RouterModule, Routes} from "@angular/router";
import { FormsModule } from '@angular/forms';
import { PluginTileComponent } from './component/plugin/plugin-tile/plugin-tile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {PluginDetailComponent} from "./component/plugin/plugin-detail/plugin-detail.component";
import {PluginFormComponent} from "./component/plugin/plugin-form/plugin-form.component";
import {HeaderComponent} from "./component/header/header.component";
import {AuthGuard} from "./auth/auth.guard";
import {LoginComponent} from "./auth/login/login.component";
import {AuthenticationService} from "./auth/authentication.service";
import {LoginGuard} from "./auth/login.guard";
import {PluginService} from "./component/plugin/plugin.service";
import {TagComponent} from "./component/tag/tag.component";
import {HttpClientModule} from "@angular/common/http";
import {NgxPaginationModule} from "ngx-pagination";
import {UserService} from "./user/user.service";
import {NgxMasonryModule} from "ngx-masonry";
import {PedalboardDetailComponent} from "./component/pedalboard/pedalboard-detail/pedalboard-detail.component";
import {PedalboardListComponent} from "./pedalboard-list/pedalboard-list.component";
import {PedalboardFormComponent} from "./component/pedalboard/pedalboard-form/pedalboard-form.component";

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [LoginGuard]},
  {
    path: '', canActivate: [AuthGuard],
    children: [

      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {path: 'home', component: HomeComponent},
      {
        path: 'pedalboards',
        children: [
          {path: '', component: PedalboardListComponent, data: {title: 'Pedalboard Feed', subtitle: 'Here be pedalboards'}},
          {path: 'add', component: PedalboardFormComponent, data: {title: 'NEW PEDALBOARD', subtitle: 'Add new pedalboard', type: 'ADD'}},
          {path: 'pedalboard/:id', component: PedalboardDetailComponent, data: {title: 'PEDALBOARD DETAILS', subtitle: ' '}},
        ]
      },
      {
        path: 'plugins',
          children: [
            {path: '', component: ListComponent, data: {title: 'PLUGINS', subtitle: 'Here be plugins'}},
            {path: 'plugin/:id', component: PluginDetailComponent, data: {title: 'PLUGIN DETAILS', subtitle: ' '}},
            {path: 'add', component: PluginFormComponent, data: {title: 'NEW PLUGIN', subtitle: 'Add new plugin', type: 'ADD'}},
            {path: 'edit/:id', component: PluginFormComponent, data: {title: 'EDIT PLUGIN', subtitle: 'Edit plugin', type: 'EDIT'}}

          ]
      },
      {path: '**', component: HomeComponent}, //replace by PageNotFoundComponent - 404 or whatever
      {path: '', component: ListComponent, data: {title: 'PLUGINS', subtitle: 'Here be plugins'}}
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    ListComponent,
    PluginTileComponent,
    PluginDetailComponent,
    PluginFormComponent,
    HeaderComponent,
    LoginComponent,
    TagComponent,
    PedalboardDetailComponent,
    PedalboardListComponent,
    PedalboardFormComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxMasonryModule
  ],
  exports: [RouterModule],
  providers: [AuthGuard, LoginGuard, AuthenticationService, PluginService, UserService
  ],
  bootstrap: [AppComponent, NavComponent]
})
export class AppModule { }
