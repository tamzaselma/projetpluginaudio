import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../user/user";

@Injectable()
export class AuthenticationService {
  allUsers: User[] = [];

  constructor(private router: Router) {}

  isAuthenticated(): boolean {
    return localStorage.getItem('loggedUser') != null;
  }

  getLoggedUser(): User {
    var loggedUser =  JSON.parse(localStorage.getItem('loggedUser'));
    return new User(loggedUser.id, loggedUser.username, loggedUser.password, loggedUser.type, loggedUser.url);
  }

  isAdminLogged(): boolean {
    return this.getLoggedUser().type === 'ADMIN';
  }

  login(username: string, password: string) {
    var self = this;
    return self.loadUsers()
      .then(() => {
        var user = self.findLoggedUser(username, password);
        if(user) {
          localStorage.setItem('loggedUser', JSON.stringify(user));
        }
      });
  }

  logout() {
    localStorage.removeItem('loggedUser');
    this.router.navigate(['login']);
  }

  private createUser(p: any) {
    var self = this;
    return new Promise(resolve => setTimeout(() => resolve(self.allUsers.push(new User(p.id, p.username, p.password, p.type, p.url))), 100));
  }

  loadUsers() {
    var self = this;
    return fetch("http://localhost:3000/users")
      .then(results => {return results.json()})
      .then(data => {
        return Promise.all(data.map((usr) => {
          return self.createUser(usr);
        }));
      });
  }

  private findLoggedUser(username: string, password: string): User {
    var self = this;
    return this.allUsers.find(user => self.usernameAndPasswordMatch(user, username, password));
  }

  private usernameAndPasswordMatch(user: User, username: string, password: string): boolean {
    return user.username === username && user.password === password;
  }

}
