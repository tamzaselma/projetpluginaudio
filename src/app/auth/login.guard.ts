import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/index";
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log("[LoginGuard]: canActivate - " + !this.authService.isAuthenticated());
    if (!this.authService.isAuthenticated()) {
      return true;
    }

    this.router.navigate(['home']);
    return true;
  }

}
