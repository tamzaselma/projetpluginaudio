import {AuthenticationService} from "../authentication.service";
import {Router} from "@angular/router";
import {Component, OnInit} from "@angular/core";

@Component({
  selector: "login",
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;

  constructor(private router: Router,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password).then(() => {
      this.loading = false;
      console.log("Is user logged: " + this.authenticationService.isAuthenticated());
      this.router.navigate(['plugins']);
    });

  }
}
