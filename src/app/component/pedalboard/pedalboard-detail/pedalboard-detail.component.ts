import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../../user/user.service";
import {User} from "../../../user/user";
import {PluginService} from "../../plugin/plugin.service";
import {Pedalboard} from "../pedalboard";

@Component({
  selector: 'app-pedalboard-detail',
  templateUrl: './pedalboard-detail.component.html',
  styleUrls: ['./pedalboard-detail.component.css']
})
export class PedalboardDetailComponent implements OnInit {

  title = "";
  subtitle = "";
  private pedalboard: Pedalboard = new Pedalboard();
  private author = new User(null, '', '', '', '');
  plugins = [];

  constructor(private route: ActivatedRoute, private pluginService: PluginService, private userService: UserService
  ) {
    this.route.params.subscribe(params => {
      if (params["id"]) {
        this.loadPedalboard(params["id"]);
      }
    });
    this.route.data.subscribe(data => {
      if (data["title"] && data["subtitle"]) {
        this.title = data["title"];
        this.subtitle = data["subtitle"];
      }
    });
  }

  ngOnInit() {
  }

  loadPedalboard(id: number) {
    this.pluginService.findPedalboardById(id).subscribe((pedalboard) => {
      this.pedalboard = pedalboard;
      this.pluginService.findPluginsByIds(pedalboard.plugins).subscribe((plugins) => {
        this.plugins = [].concat(plugins);
      })
      this.userService.findUserByUsername(pedalboard.author).subscribe((author) => {
        this.author = author;
      })
    });
  }

}
