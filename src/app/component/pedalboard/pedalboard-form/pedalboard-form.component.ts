import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../../auth/authentication.service";
import {UserService} from "../../../user/user.service";
import {User} from "../../../user/user";
import {Pedalboard} from "../pedalboard";
import {PluginService} from "../../plugin/plugin.service";
import {DatePipe} from "@angular/common";
import {Plugin} from "../../plugin/plugin";

@Component({
  selector: 'app-pedalboard-form',
  templateUrl: './pedalboard-form.component.html',
  styleUrls: ['./pedalboard-form.component.css'],
  providers: [DatePipe]
})
export class PedalboardFormComponent implements OnInit {

  title = "";
  subtitle = "";
  type = null;
  users = [];
  private authorDisabled = false;
  pedalboard: Pedalboard = new Pedalboard();
  allPlugins: Plugin[] = [];
  selectedPlugins: number[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pluginService: PluginService,
    private userService: UserService,
    private authService: AuthenticationService,
    private datePipe: DatePipe)
  {
    this.route.data.subscribe(data => {
      if (data["title"] && data["subtitle"] && data["type"]) {
        this.title = data["title"];
        this.subtitle = data["subtitle"];
        this.type = data["type"];
      }
    });
    this.authorDisabled = !this.authService.isAdminLogged();
  }

  ngOnInit() {
    this.fillAuthor();
    this.loadPlugins();
  }

  onSubmit(){
    if(this.type == "ADD") {
      this.pedalboard.date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
      this.pedalboard.plugins = this.selectedPlugins;
      this.pluginService.addPedalboard(this.pedalboard).subscribe(() => {
        this.router.navigate(['/pedalboards']);
      });
    }
  }

  onPluginSelected(pluginId: number) {
    console.log(pluginId)
    this.selectedPlugins.push(pluginId);
  }

  loadPlugins(){
    this.pluginService.getPlugins()
      .subscribe((data: Plugin[]) => {
        this.allPlugins = [];
        data.forEach((p) => {
          this.allPlugins.push(p);
        })
      });
  }

  fillAuthor() {
    if (this.authorDisabled && !this.pedalboard.author) {
      this.pedalboard.author = this.authService.getLoggedUser().username;
      this.users.push(this.authService.getLoggedUser().username);
    } else {
      this.userService.getUsers().subscribe((data: User[]) => {
        this.users = data.map((user) => user.username);
      })
    }
  }

}
