export class Pedalboard {
  constructor(
    public id?: string,
    public name?: string,
    public author?: string,
    public date?: string,
    public description?: string,
    public plugins?: number[],
    public screenshot?: string,
    public audio?: string,
  ) {}
}
