export class Button {
  constructor(
    public id: number,
    public name: string,
    public value: string,
    public min: string,
    public max: string
  ) {}
}
