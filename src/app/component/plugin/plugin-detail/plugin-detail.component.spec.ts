import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlugindDataComponent } from './plugin-detail.component';

describe('PlugindDataComponent', () => {
  let component: PlugindDataComponent;
  let fixture: ComponentFixture<PlugindDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlugindDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlugindDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
