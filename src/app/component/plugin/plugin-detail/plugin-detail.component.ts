import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Plugin} from "../plugin";
import {AuthGuard} from "../../../auth/auth.guard";
import {PluginService} from "../plugin.service";
import {AuthenticationService} from "../../../auth/authentication.service";
import {UserService} from "../../../user/user.service";
import {User} from "../../../user/user";
import {Pedalboard} from "../../pedalboard/pedalboard";

@Component({
  selector: 'app-plugin-detail',
  templateUrl: './plugin-detail.component.html',
  styleUrls: ['./plugin-detail.component.css']
})
export class PluginDetailComponent implements OnInit {

  title = "";
  subtitle = "";
  private plugin: Plugin = new Plugin();
  private author = new User(null, '', '', '', '');
  pedalboards: Pedalboard[] = [];

  constructor(private route: ActivatedRoute, private router: Router, private pluginService: PluginService, private authService: AuthenticationService, private userService: UserService
  ) {
    this.route.params.subscribe(params => {
      if (params["id"]) {
        this.loadPlugin(params["id"]);
      }
    });
    this.route.data.subscribe(data => {
      if (data["title"] && data["subtitle"]) {
        this.title = data["title"];
        this.subtitle = data["subtitle"];
      }
    });
  }

  ngOnInit() {
  }

  loadPlugin(id: number) {
    this.pluginService.findPluginById(id).subscribe((plugin) => {
      this.plugin = plugin;
      this.userService.findUserByUsername(plugin.author).subscribe((author) => {
        this.author = author;
      })
      this.pluginService.findPedalboardsByPluginId(plugin.id).subscribe((result: Pedalboard[]) => {
        this.pedalboards = result;
        console.log("Pedalboard")
        console.log(result);
      });
    });
  }

  onDelete() {
    this.pluginService.deletePlugin(this.plugin).subscribe(() => {
      this.router.navigate(['/plugins']);
    });
  }

  onUpdate() {
    this.router.navigate(['plugins/edit/' + this.plugin.id]);
  }

  canEdit() {
    return this.pluginService.isAtuhor(this.authService.getLoggedUser(), this.plugin) || this.authService.isAdminLogged();
  }

  canDelete() {
    return this.pluginService.isAtuhor(this.authService.getLoggedUser(), this.plugin) || this.authService.isAdminLogged();
  }

}
