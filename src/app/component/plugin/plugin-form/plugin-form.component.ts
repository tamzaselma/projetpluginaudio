import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Plugin} from "../plugin";
import {PluginService} from "../plugin.service";
import { HttpClient } from '@angular/common/http';
import {AuthenticationService} from "../../../auth/authentication.service";
import {UserService} from "../../../user/user.service";
import {User} from "../../../user/user";
import {Button} from "../button";

@Component({
  selector: 'app-plugin-form',
  templateUrl: './plugin-form.component.html',
  styleUrls: ['./plugin-form.component.css']
})
export class PluginFormComponent implements OnInit {

  title = "";
  subtitle = "";
  type = null;
  tags = "";
  image: File;
  users = [];
  private authorDisabled = false;
  plugin: Plugin = new Plugin();
  controls: Button[] = [];

  constructor(private route: ActivatedRoute, private router: Router, private pluginService: PluginService, private http: HttpClient, private userService: UserService, private authService: AuthenticationService) {
    this.route.data.subscribe(data => {
      if (data["title"] && data["subtitle"] && data["type"]) {
        this.title = data["title"];
        this.subtitle = data["subtitle"];
        this.type = data["type"];
      }
    });
    this.authorDisabled = !this.authService.isAdminLogged();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id']) {
        this.loadPlugin(+params['id']);
      }
    });
    this.fillAuthor();
  }

  onSubmit(){
    this.plugin.tags = this.pluginService.parseBySpace(this.tags);
    this.plugin.buttons = this.controls;

    if(this.type == "ADD") {
      this.pluginService.addPlugin(this.plugin).subscribe(() => {
        this.router.navigate(['/plugins']);
      });
    } else {
      this.pluginService.updatePlugin(this.plugin).subscribe(() => {
        this.router.navigate(['/plugins']);
      })
    }
  }

  onFileChanged(event) {
    this.image = event.target.files[0]
  }

  fillAuthor() {
    if (this.authorDisabled && !this.plugin.author) {
      this.plugin.author = this.authService.getLoggedUser().username;
      this.users.push(this.authService.getLoggedUser().username);
    } else {
      this.userService.getUsers().subscribe((data: User[]) => {
        this.users = data.map((user) => user.username);
      })
    }
  }

  loadPlugin(id: number) {
    var self = this;
    this.pluginService.findPluginById(id)
      .subscribe(data => {
        self.plugin = data;
        data.tags.forEach((tag) => {
          self.tags += tag + ' ';
        })
          self.controls = data.buttons;
      });
  }

  onAddControls() {
    this.controls.push(new Button(this.controls.length, "","","",""));
  }

  onDeleteControl(controlToRemove: Button) {
    this.controls = this.controls.filter(control => control !== controlToRemove);
  }

}
