import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PluginTileComponent } from './plugin-tile.component';

describe('PluginTileComponent', () => {
  let component: PluginTileComponent;
  let fixture: ComponentFixture<PluginTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PluginTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PluginTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
