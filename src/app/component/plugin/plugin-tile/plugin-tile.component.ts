import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Plugin} from "../plugin";

@Component({
  selector: 'app-plugin-tile',
  templateUrl: './plugin-tile.component.html',
  styleUrls: ['./plugin-tile.component.css']
})
export class PluginTileComponent implements OnInit {

  @Input() plugin: Plugin;
  @Input() enableSelect: boolean;
  @Output() selected = new EventEmitter<number>(true);
  @Output() filterByTag = new EventEmitter<string>(true);
  @Output() filterByAuthor = new EventEmitter<string>(true);
  SELECT_LABEL = "Select";

  constructor() {
  }

  ngOnInit() {
  }

  onFilterByTagEvent(filterTag: string) {
    this.filterByTag.emit(filterTag);
  }

  onFilterByAuthor(author: string) {
    this.filterByAuthor.emit(author);
  }

  onSelect() {
    this.SELECT_LABEL = this.SELECT_LABEL === "Select"? "Remove" : "Select";
    this.selected.emit(this.plugin.id);
  }

}
