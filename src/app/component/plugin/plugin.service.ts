import {Injectable} from "@angular/core";
import {Plugin} from "./plugin"
import {User} from "../../user/user";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/internal/operators";
import {Pedalboard} from "../pedalboard/pedalboard";

@Injectable({
  providedIn: 'root'
})
export class PluginService {

  constructor(private http: HttpClient) {
  }

  getPlugins() {
    console.log("[PluginService]: getPlugins");
    return this.http.get('http://localhost:3000/plugins');
  }

  addPlugin(plugin: Plugin) {
    return this.http.post<Plugin>("http://localhost:3000/plugins", plugin);
  }

  updatePlugin(plugin: Plugin) {
    return this.http.put("http://localhost:3000/plugins/" + plugin.id, plugin);
  }

  deletePlugin(plugin: Plugin) {
    return this.http.delete("http://localhost:3000/plugins/" + plugin.id);
  }

  getPluginPlainText(plugin: Plugin) {
    var text = plugin.name + " " + plugin.description + " " + plugin.author;
    plugin.tags.forEach((tag) => {
      text += " " + tag;
    })
    return text.toLowerCase();
  }

  findPluginsByTag(tag: string) {
    console.log("[Plugin Service]: " + tag)
    return this.http.get('http://localhost:3000/plugins')
      .pipe(map((data) => this.filterByTag(data, tag)));
  }

  findPluginsByAuthorUsername(username: string) {
    return this.http.get('http://localhost:3000/plugins?author=' + username);
  }

  findPluginById(id: number) {
    return this.http.get<Plugin>('http://localhost:3000/plugins?id=' + id).pipe(
        map(plugins => plugins[0])
    );
  }

  findPluginsByIds(ids: number[]) {
    var idString = ids.join("&id=");
    return this.http.get<Plugin[]>('http://localhost:3000/plugins?id=' + idString);
  }

  getPedalboards() {
    return this.http.get('http://localhost:3000/pedalboards');
  }

  addPedalboard(pedalboard: Pedalboard) {
    return this.http.post<Pedalboard>("http://localhost:3000/pedalboards", pedalboard);
  }

  findPedalboardById(id: number) {
    return this.http.get<Pedalboard>('http://localhost:3000/pedalboards?id=' + id).pipe(
      map(pedalboards => pedalboards[0])
    );
  }

  findPedalboardsByPluginId(id: number) {
    return this.http.get<Pedalboard[]>('http://localhost:3000/pedalboards').pipe(
      map(pedalboards => this.filterByPluginId(pedalboards, id))
    );
  }

  getPedalboardPlainText(pedalboard: Pedalboard) {
    var text = pedalboard.name + " " + pedalboard.description + " " + pedalboard.author;
    return text.toLowerCase();
  }

  private filterByPluginId(resp: Pedalboard[], id: number) {
    var result = resp
      .filter((p) => {return p.plugins.some(pId => pId == id)});
    console.log("Filter result")
    console.log(result)
    return result;
  }

  private filterByTag(resp: any, tag: string) {
    return resp
      .filter(p => {
        var plugin = new Plugin(p.id, p.name, p.image, p.author, p.description, p.tags);
        return this.hasTag(plugin, tag);
      });
  }

  private hasTag(plugin: Plugin, tag: string) {
    return plugin.tags.some((t) => t.toLowerCase().trim() === tag.toLowerCase().trim());
  }

  parseBySpace(input: string): string[] {
    return input.trim().split(" ").filter((word) => word != "");
  }

  isAtuhor(user: User, plugin: Plugin) {
    return user.username == plugin.author;
  }

}
