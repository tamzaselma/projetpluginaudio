import {Button} from "./button";

export class Plugin {
  constructor(
    public id?: number,
    public name?: string,
    public image?: string,
    public author?: string,
    public description?: string,
    public tags?: string[],
    public buttons?: Button[]
  ) {}

}
