import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: 'app-tag',
  template: '<span class="plugin-category" (click)="onFilterByTag()">{{tag}}</span>',
  styles: ['.plugin-category {cursor: pointer;}']
})
export class TagComponent {
  @Input() tag;
  @Output() filterByTag = new EventEmitter<string>(true);

  onFilterByTag() {
    this.filterByTag.emit(this.tag);
  }
}
