import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Plugin} from "../component/plugin/plugin";
import {ActivatedRoute} from "@angular/router";
import {PluginService} from "../component/plugin/plugin.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  allPlugins: Plugin[] = [];
  searchInput = " ";
  title = "";
  subtitle = "";
  @Input() noHeader: boolean = false;
  @Input() enableSelect: boolean;
  @Output() pluginSelected = new EventEmitter<number>(true);

  itemsPerPage: number = 6;
  paginationOptions = [2, 5, 6, 10, 15];
  ITEMS_PER_PAGE_LABEL = "Number of plugins per page";

  constructor(
    private route: ActivatedRoute,
    private pluginService: PluginService
  ) {
    this.route.data.subscribe(data => {
      if (data["title"] && data["subtitle"]) {
        this.title = data["title"];
        this.subtitle = data["subtitle"];
      }
    });
  }

  ngOnInit() {
    this.loadPlugins();
  }

  loadPlugins(){
    this.pluginService.getPlugins()
      .subscribe((data: Plugin[]) => {
        this.allPlugins = [];
        data.forEach((p) => {
          this.allPlugins.push(p);
      })
    });
  }

  ngAfterViewInit(){
    this.loadPlugins();
  }

  onSearch() {
    var self = this;
    this.allPlugins = [];
    this.pluginService.parseBySpace(self.searchInput.toLowerCase()).forEach((word) => {
      this.pluginService.getPlugins()
        .subscribe((data: Plugin[]) => {
          data.forEach((plugin) => {
            if (self.pluginService.getPluginPlainText(plugin).toLowerCase().includes(word)) {
              self.allPlugins.push(plugin);
            }
          });
        });
    })
  }

  onPluginResetSearch() {
    this.loadPlugins();
  }

  onFilterByTagEvent(filterTag: string) {
    this.pluginService.findPluginsByTag(filterTag)
      .subscribe((plugins: Plugin[]) => {
        this.allPlugins = [].concat(plugins);
      });
  }

  onFilterByAuthorEvent(filterAuthorUserame: string) {
    this.pluginService.findPluginsByAuthorUsername(filterAuthorUserame)
      .subscribe((plugins: Plugin[]) => {
        this.allPlugins = [].concat(plugins);
      });
  }

  onPluginSelected(pluginId: number) {
    this.pluginSelected.emit(pluginId);
  }

}
