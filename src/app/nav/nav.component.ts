import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../auth/authentication.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  tabs = [
    {name: 'pedalboards', marked: false, routerLink: 'pedalboards'},
    {name: 'plugins', marked: false, routerLink: 'plugins'},
    {name: 'forum', marked: true, routerLink: 'home'}
  ]

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
  }

  isLoggedIn(): boolean {
    return this.authService.isAuthenticated();
  }

  onLogout() {
    this.authService.logout();
  }

}
