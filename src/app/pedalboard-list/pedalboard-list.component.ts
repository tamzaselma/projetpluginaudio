import {Component, OnInit} from "@angular/core";
import {Pedalboard} from "../component/pedalboard/pedalboard";
import {ActivatedRoute, Router} from "@angular/router";
import {PluginService} from "../component/plugin/plugin.service";

@Component({
  selector: 'app-pedalboard-list',
  templateUrl: './pedalboard-list.component.html',
  styleUrls: ['./pedalboard-list.component.css']
})
export class PedalboardListComponent implements OnInit {
  allPedalboards: Pedalboard[] = [];
  searchInput = " ";
  title = "";
  subtitle = "";

  constructor(
    private route: ActivatedRoute,
    private pluginService: PluginService,
    private router: Router,
  ) {
    this.route.data.subscribe(data => {
      if (data["title"] && data["subtitle"]) {
        this.title = data["title"];
        this.subtitle = data["subtitle"];
      }
    });
  }

  ngOnInit() {
    this.loadPedalboards();
  }

  loadPedalboards(){
    this.pluginService.getPedalboards()
      .subscribe((data: Pedalboard[]) => {
        this.allPedalboards = [];
        data.forEach((p) => {
          this.allPedalboards.push(p);
        })
      });
  }

  onViewMore(id: number) {
    this.router.navigate(['pedalboards/pedalboard', id]);
  }

  onSearch() {
    var self = this;
    this.allPedalboards = [];
    this.pluginService.parseBySpace(self.searchInput.toLowerCase()).forEach((word) => {
      this.pluginService.getPedalboards()
        .subscribe((data: Pedalboard[]) => {
          data.forEach((pedalboard) => {
            if (self.pluginService.getPedalboardPlainText(pedalboard).toLowerCase().includes(word)) {
              self.allPedalboards.push(pedalboard);
            }
          });
        });
    })
  }

  onResetSearch() {
    this.loadPedalboards();
  }

}
