import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/internal/operators";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getUsers() {
    console.log("[UserService]: getUsers");
    return this.http.get<User[]>('http://localhost:3000/users');
  }

  findUserByUsername(username: string) {
    return this.http.get<User>('http://localhost:3000/users?username=' + username).pipe(
      map(results => results[0])
    );
  }


}
