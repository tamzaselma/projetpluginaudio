export class User {
  constructor(
    public id: number,
    public username: string,
    public password: string,
    public type: string,
    public url: string
  ) {}
}
